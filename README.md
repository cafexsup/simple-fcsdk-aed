##Minimum App (PHP)

###QuickStart

This sample is the minimum viable code required to create, connect to and push data to an AED Topic. The application consists of a basic web page (index.html) with embedded CafeX JavaScript libraries. Session provisioning is handled via an ajax call to a PHP script (session.php) running on a server.

To run this sample, you will need:

1. A CafeX FAS & FCSDK server
2. To edit the two files (index.html and session.php) to point to the IP of your server.
3. To deploy a webserver (e.g. MAMP / WAMP) to host the files and run the php script.



